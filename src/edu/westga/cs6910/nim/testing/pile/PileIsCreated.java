package edu.westga.cs6910.nim.testing.pile;
/**
 * These are the testing cases for the class pile
 */
import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.Pile;

public class PileIsCreated {

	/**
	 * This test the constructor of the pile class with 1 stick
	 */
	@Test
	public void testShouldCreatePileWith1Stick() {
		Pile oneStickPile = new Pile(1);
		assertEquals("Pile size: 1", oneStickPile.toString());
	}
		
	/**
	 * This test the constructor with 0 sticks
	 */
	@Test
	public void testShouldCreatePileWith0Sticks() {
		Pile oneStickPile = new Pile(0);
		assertEquals("Pile size: 0", oneStickPile.toString());
	}
		
	/**
	 * This test the constructor with 0 sticks
	 */
	@Test
	public void testShouldCreatePileWithNegative1Stick() {
		Pile oneStickPile = new Pile(-1);
		assertEquals("Pile size: -1", oneStickPile.toString());
		
	}

}
