package edu.westga.cs6910.nim.testing.pile;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.Pile;

public class PileWhenRemoveSticks {

	/**
	 * This test the removeSticks method
	 */
	@Test
	public void testShouldHave1StickLeftAfterRemoving1StickFrom2StickPile() {
		Pile twoStickPile = new Pile(2);
		twoStickPile.removeSticks(1);
		assertEquals(1, twoStickPile.getSticksLeft());
		
	}
	
	/**
	 * This test the removeSticks method
	 */
	@Test
	public void testShouldHave2StickLeftAfterRemoving1StickFrom3StickPile() {
		Pile twoStickPile = new Pile(3);
		twoStickPile.removeSticks(1);
		assertEquals(2, twoStickPile.getSticksLeft());
		
	}

	/**
	 * This test the removeSticks method
	 */
	@Test
	public void testShouldHave8StickLeftAfterRemoving2StickFrom10StickPile() {
		Pile twoStickPile = new Pile(10);
		twoStickPile.removeSticks(2);
		assertEquals(8, twoStickPile.getSticksLeft());
		
	}
}
