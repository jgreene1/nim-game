package edu.westga.cs6910.nim.testing.pile;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.Pile;

public class PileGetSticksLeft {

	/** 
	 * This will test the getSticksLeft method
	 */
	@Test
	public void testWith1StickPileShouldHave1StickLeft() {
		Pile oneStickPile = new Pile(1);
		assertEquals(1,oneStickPile.getSticksLeft());
	}
	
	/** 
	 * This will test the getSticksLeft method
	 */
	@Test
	public void testWith0StickPileShouldHave0StickLeft() {
		Pile zeroStickPile = new Pile(0);
		assertEquals(0,zeroStickPile.getSticksLeft());
	}
	
	/** 
	 * This will test the getSticksLeft method
	 */
	@Test
	public void testWith10StickPileShouldHave8StickLeft() {
		Pile twoStickPile = new Pile(2);
		assertEquals(2,twoStickPile.getSticksLeft());
	}

}
