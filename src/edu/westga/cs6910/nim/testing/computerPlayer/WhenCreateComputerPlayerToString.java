package edu.westga.cs6910.nim.testing.computerPlayer;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class WhenCreateComputerPlayerToString {

	/**
	 * This test the constructor of the computer player as well as the toString 
	 */
	@Test
	public void WhenCreateComputerPlayerToString1() {
		ComputerPlayer computer = new ComputerPlayer(0,null);
		assertEquals("Computer Player named: Simple computer with 0 sticks to take", computer.toString());
		
	}
	
	/**
	 * This test checks to see if the to string changes from the abstract sticksToTake/ This will also test the number of sticks to take for the computer player
	 */
	@Test
	public void WhenCreateComputerPlayerToString2() {
		HumanPlayer player = new HumanPlayer("me",2,null); 
		ComputerPlayer computer = new ComputerPlayer(0, null);
		Game firstGame = new Game();
		firstGame.startNewGame(player, computer);
		player.setNumberSticksToTake(2); 
		firstGame.play();
		computer.setNumberSticksToTake(1);
		firstGame.play();
		assertEquals("Computer Player named: Simple computer with 1 sticks to take", computer.toString());
	}
	
	/**
	 * This test checks to see if to string will change after computer takes a turn/ This will also test the number of sticks to take for the computer player
	 */
	@Test
	public void WhenCreateComputerPlayerToStringAfterComputerTurn() {
		HumanPlayer player = new HumanPlayer("me",2,null); //is overriding 
		ComputerPlayer computer = new ComputerPlayer(0, null);
		Game firstGame = new Game();
		firstGame.startNewGame(player, computer);
		player.setNumberSticksToTake(2); 
		firstGame.play();
		computer.setNumberSticksToTake(1);
		firstGame.play();
		player.setNumberSticksToTake(3);
		firstGame.play();
		computer.setNumberSticksToTake(2);
		assertEquals("Computer Player named: Simple computer with 2 sticks to take", computer.toString());
	}
}
