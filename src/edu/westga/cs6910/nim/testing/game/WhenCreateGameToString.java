package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class WhenCreateGameToString {

	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGameOverTrueToStringComputerWins() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		theHuman.setNumberSticksToTake(2);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		firstGame.isGameOver();
		assertEquals("Game over! Winner: Simple computer", firstGame.toString());
	}
	
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGameOverTrueToStringHumanWins() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(2);
		firstGame.play();
		theHuman.setNumberSticksToTake(2);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		theHuman.setNumberSticksToTake(1);
		firstGame.play();
		firstGame.isGameOver();
		assertEquals("Game over! Winner: me", firstGame.toString());
	}
	
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGameOverTrueToStringComputerWinsAgain() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(3);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		theHuman.setNumberSticksToTake(1);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		firstGame.isGameOver();
		assertEquals("Game over! Winner: Simple computer", firstGame.toString());
	}
}