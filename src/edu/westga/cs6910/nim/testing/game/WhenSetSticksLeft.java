package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.Game;

public class WhenSetSticksLeft {

	@Test
	public void setSticksLeft() {
		Game firstGame = new Game();
		firstGame.setSticksLeft(5);
		assertEquals(5, firstGame.getSticksLeft());
	}

	@Test
	public void setSticksLeftTest2() {
		Game firstGame = new Game();
		firstGame.setSticksLeft(10);
		assertEquals(10, firstGame.getSticksLeft());
	}

	@Test
	public void setSticksLeftTest3() {
		Game firstGame = new Game();
		firstGame.setSticksLeft(50);
		assertEquals(50, firstGame.getSticksLeft());
	}

}
