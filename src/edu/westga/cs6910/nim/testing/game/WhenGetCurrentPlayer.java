package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;
import edu.westga.cs6910.nim.model.Pile;

public class WhenGetCurrentPlayer {

	/**
		 * This will test to see if human player is the current player 
		 */
		@Test
		public void testShouldGetHumanAsCurrentPlayerAtBeginning() {
			Game firstGame = new Game();
			HumanPlayer theHuman = new HumanPlayer("Me", 10, firstGame.getPile());
			ComputerPlayer theComputer = new ComputerPlayer(0, firstGame.getPile());
			firstGame.getHumanPlayer().setNumberSticksToTake(3);
			theHuman.setNumberSticksToTake(3); 
			firstGame.startNewGame(theHuman, theComputer);
			firstGame.play();
			assertEquals(theHuman.toString() , firstGame.getHumanPlayer().toString());
		}

		/**
		 * This will test to see if computer player is the current player.
		 */
		@Test
		public void testShouldGetComputerAsCurrentPlayer() {
			Game firstGame = new Game();
			HumanPlayer theHuman = new HumanPlayer("me",10, firstGame.getPile());
			ComputerPlayer theComputer = new ComputerPlayer(0, firstGame.getPile());
			firstGame.getHumanPlayer().setNumberSticksToTake(2);
			theHuman.setNumberSticksToTake(2);
			firstGame.startNewGame(theHuman, theComputer);
			firstGame.play();
			assertEquals(theComputer.toString() , firstGame.getCurrentPlayer().toString());
		}
		
		/**
		 * This will test after a couple turns if it goes back to human player. 
		 */
		@Test
		public void testShouldGethumanAsCurrentPlayerAfterComputerTurn() {
			Game firstGame = new Game();
			HumanPlayer theHuman = new HumanPlayer("Me",10, firstGame.getPile());
			ComputerPlayer theComputer = new ComputerPlayer(0,firstGame.getPile());
			firstGame.getHumanPlayer().setNumberSticksToTake(2);
			theHuman.setNumberSticksToTake(2);
			firstGame.startNewGame(theHuman, theComputer);
			firstGame.play();
			firstGame.getComputerPlayer().setNumberSticksToTake(1);
			theComputer.setNumberSticksToTake(1);
			firstGame.play();
			assertEquals(theHuman.toString() , firstGame.getHumanPlayer().toString());
		}

}
