package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class WhenIsGameOver {


	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGameOverTrue() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		theHuman.setNumberSticksToTake(2);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		assertEquals(true, firstGame.isGameOver());

	}
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGameOverFalse() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		theHuman.setNumberSticksToTake(1);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		assertEquals(false, firstGame.isGameOver());

	}
	
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGameOverTrueTestTwo() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(4);
		firstGame.play();
		theHuman.setNumberSticksToTake(1);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		assertEquals(true, firstGame.isGameOver());
	}
}
