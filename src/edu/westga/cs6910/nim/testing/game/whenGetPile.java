package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class whenGetPile {

	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGetPileEquals1() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		theHuman.setNumberSticksToTake(2);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		assertEquals("Pile size: 1", firstGame.getPile().toString());

	}
	
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGetPileEqual2() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		theHuman.setNumberSticksToTake(1);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		assertEquals("Pile size: 2", firstGame.getPile().toString());

	}
	
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenGetPileEqual3() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(2);
		firstGame.play();
		theHuman.setNumberSticksToTake(1);
		firstGame.play();
		theComputer.setNumberSticksToTake(1);
		firstGame.play();
		assertEquals("Pile size: 3", firstGame.getPile().toString());

	}
}
