package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class WhenGetSticksLeftAfterTurn {

	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenSticksLeftAfterOneTurn() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(1,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		assertEquals(7, firstGame.getPile().getSticksLeft());

	}
	
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenSticksLeftAfterTwoTurns() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		assertEquals(4 , firstGame.getSticksLeft());

	}
	
	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void whenSticksLeftAfterThreeTurns() {
		HumanPlayer theHuman = new HumanPlayer("me",2, null);
		ComputerPlayer theComputer = new ComputerPlayer(3,null);
		Game firstGame = new Game();
		theHuman.setNumberSticksToTake(2);
		firstGame.startNewGame(theHuman, theComputer);
		firstGame.play();
		theComputer.setNumberSticksToTake(3);
		firstGame.play();
		theHuman.setNumberSticksToTake(2);
		firstGame.play();
		assertEquals(2 , firstGame.getSticksLeft());

	}
}
