package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class GameCreatedGetHumanPlayer {

	/**
	 * This will test changing the name of the player to me
	 */
	@Test
	public void testShouldGetHumanPlayer() {
		HumanPlayer theHuman = new HumanPlayer("Me", 0, null);
		Game firstGame = new Game();
		assertEquals(theHuman.toString() , firstGame.getHumanPlayer().toString());
	}
	
}
