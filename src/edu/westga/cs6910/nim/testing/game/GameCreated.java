package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class GameCreated {


	/**
	 * This will test the constructor of the Game method and check if initial size is established
	 */
	@Test
	public void testShouldCreateGameAndReturnInitialPileSize() {
		Game firstGame = new Game();
		assertEquals(" Pile size: 9" , firstGame.toString());
	}
	
	/**
	 * This will test the constructor of the Game method and check if a human player is created
	 */
	@Test
	public void testShouldCreateGameAndReturnTheHumanPlayer() {
		Game firstGame = new Game();
		HumanPlayer theHuman = new HumanPlayer("Me", 0, firstGame.getPile());
		assertEquals(theHuman.toString() , firstGame.getHumanPlayer().toString());
	}
	
	/**
	 * This will test the constructor of the Game method and check that a computer is created
	 */
	@Test
	public void testShouldCreateGameAndReturnTheComputerPlayer() {
		Game firstGame = new Game();
		ComputerPlayer theComputer = new ComputerPlayer(1, firstGame.getPile());
		assertEquals(theComputer.toString() , firstGame.getComputerPlayer().toString());
	}

}
