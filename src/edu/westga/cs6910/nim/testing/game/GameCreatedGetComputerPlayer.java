package edu.westga.cs6910.nim.testing.game;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class GameCreatedGetComputerPlayer {

	/**
	 * This will test if computer player was created
	 */
	@Test
	public void testShouldGetComputerPlayer() {
		Game firstGame = new Game();
		assertEquals("Computer Player named: Simple computer with 1 sticks to take" , firstGame.getComputerPlayer().toString());
	}
	

}
