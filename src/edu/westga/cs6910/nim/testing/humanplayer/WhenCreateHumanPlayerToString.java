package edu.westga.cs6910.nim.testing.humanplayer;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.HumanPlayer;

public class WhenCreateHumanPlayerToString {

	/**
	 * This test the constructor of the human player as well as the toString 
	 */
	@Test
	public void WhenCreateHumanPlayerToString1() {
		HumanPlayer player = new HumanPlayer("me",0,null);
		assertEquals("Human Player named: me with 0 sticks to take", player.toString());
		
	}
	
	/**
	 * This test checks to see if the to string changes from the abstract sticksToTake/ This will also test the number of sticks to take for the human player
	 */
	@Test
	public void WhenCreateHumanPlayerToString2() {
		HumanPlayer player = new HumanPlayer("me",2,null); //is overriding 
		ComputerPlayer computer = new ComputerPlayer(0, null);
		Game firstGame = new Game();
		firstGame.startNewGame(player, computer);
		player.setNumberSticksToTake(2); 
		firstGame.play();
		assertEquals("Human Player named: me with 2 sticks to take", player.toString());
	}
	
	/**
	 * This test checks to see if to string will change after computer takes a turn/ This will also test the number of sticks to take for the human player
	 */
	@Test
	public void WhenCreateHumanPlayerToStringAfterComputerTurn() {
		HumanPlayer player = new HumanPlayer("me",2,null); //is overriding 
		ComputerPlayer computer = new ComputerPlayer(0, null);
		Game firstGame = new Game();
		firstGame.startNewGame(player, computer);
		player.setNumberSticksToTake(2); 
		firstGame.play();
		computer.setNumberSticksToTake(1);
		firstGame.play();
		player.setNumberSticksToTake(3);
		firstGame.play();
		assertEquals("Human Player named: me with 3 sticks to take", player.toString());
	}
	


}
