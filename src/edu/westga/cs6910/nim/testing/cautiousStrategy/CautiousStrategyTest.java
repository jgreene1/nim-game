package edu.westga.cs6910.nim.testing.cautiousStrategy;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.cs6910.nim.model.CautiousStrategy;
import edu.westga.cs6910.nim.model.Pile;
/**
 * 
 * @author Jonathan Greene
 * @version 6/22/2014
 *
 */
public class CautiousStrategyTest {

	/**
	 * This test checks to see if howManySticks returns 1
	 */
	@Test
	public void whenCreateCautiousStrategyTest1() {
		Pile pile = new Pile(9);
		CautiousStrategy stra = new CautiousStrategy();
		assertEquals(1, stra.howManySticks(pile) );
	}
	
	/**
	 * This test checks to see if howManySticks returns 1
	 */
	@Test
	public void whenCreateCautiousStrategyTest2() {
		Pile pile = new Pile(10);
		CautiousStrategy stra = new CautiousStrategy();
		assertEquals(1, stra.howManySticks(pile) );
	}

	/**
	 * This test checks to see if howManySticks returns 1
	 */
	@Test
	public void whenCreateCautiousStrategyTest3() {
		Pile pile = new Pile(3);
		CautiousStrategy stra = new CautiousStrategy();
		assertEquals(1, stra.howManySticks(pile) );
	}


}
