package edu.westga.cs6910.nim.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;

import edu.westga.cs6910.nim.model.CautiousStrategy;
import edu.westga.cs6910.nim.model.ComputerPlayer;
import edu.westga.cs6910.nim.model.Game;
import edu.westga.cs6910.nim.model.GreedyStrategy;
import edu.westga.cs6910.nim.model.Player;
import edu.westga.cs6910.nim.model.RandomStrategy;

/**
 * Defines a GUI for the 1-pile Nim game.
 *  
 * @author CS 6910 
 * @author Jonathan Greene
 * @version Summer 2014
 */
public class Gui {

	private JFrame theFrame;
	private Container contentPane;	
	
	private JPanel pnlHumanPlayer;
	private JPanel pnlComputerPlayer;
	private JPanel pnlGameInfo;
	private JPanel pnlChooseFirstPlayer;
	
	private Game theGame;

	
	
	/**
	 * Creates a Gui object to provide the view for the specified
	 * Game model object.
	 * 
	 * @param theGame	the domain model object representing the Nim game
	 * 
	 * @requires theGame != null
	 * @ensures	 the GUI is displayed
	 */
	public Gui(Game theGame) {
		this.theGame = theGame;
		this.createAndShowGUI();
	}
	
	
	
	//****************************** private helper methods *******************

	private void createAndShowGUI() {
		this.theFrame = new JFrame("Simple Nim");
		
		this.theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.theFrame.setLayout(new BorderLayout());
		
		this.contentPane = this.theFrame.getContentPane();
		this.buildContentPane();
		this.buildMenuBar();


		this.theFrame.setMinimumSize(new Dimension(800, 200));
		this.theFrame.pack();
		this.theFrame.setVisible(true);
	}
	
	
	/**
	 * This helper method builds the Menu bar for the application
	 */
	private void buildMenuBar() {	
	
		JMenuBar menuBar = new JMenuBar();
		this.theFrame.setJMenuBar(menuBar);
		menuBar.add(this.fileMenu());
		menuBar.add(this.settingMenu());
		menuBar.setVisible(true);
	}

	/**
	 * This helper method builds the file menu
	 * @return returns the file menu
	 */
	private JMenu fileMenu() {
			
			//Creates File and settings button for menu
			JMenu file = new JMenu("File");
			file.setMnemonic(KeyEvent.VK_F);
	
			//Exits the application
			JMenuItem exitItem = new JMenuItem("Exit");
			exitItem.setMnemonic(KeyEvent.VK_X);
			exitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
			exitItem.setToolTipText("Exit application");
			file.add(exitItem);
			exitItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed  (ActionEvent event) {
					System.exit(0);
				}
			});
			//New Game Menu button
			JMenuItem newGameItem = new JMenuItem("New Game");
			newGameItem.setMnemonic(KeyEvent.VK_N);
			newGameItem.setToolTipText("Start New Game");
			file.add(newGameItem);
			newGameItem.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent n) {
					
					Thread newGame = new Thread("New Game") {
						public void run() {
	
							new Gui(new Game());
						}
					};
					
					newGame.start();
					theFrame.dispose();
					
					
					
				}
				
			});
			
			return file;
		}
	/**
	 * This helper method creates the settings menu 
	 * @return returns the setting menu
	 */
	private JMenu settingMenu() {
			JMenu settings = new JMenu("Settings");
			settings.setMnemonic(KeyEvent.VK_S);
			settings.add(this.computerPlayerMenu());
			return settings;
		}
	/**
	 * This helper method creates the computerPlayer sub menu of settings and should be added to the settings menu	
	 * @return the computer players menu
	 */
	private JMenuItem computerPlayerMenu() {
			JMenu sComputerPlayer = new JMenu("Computer Player");
			sComputerPlayer.setMnemonic(KeyEvent.VK_P);
			sComputerPlayer.add(this.cautiousItem()).addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					CautiousStrategy cautious = new CautiousStrategy();
					theGame.getComputerPlayer().setStrategy(cautious);
					
				}
			});
			sComputerPlayer.add(this.greedyItem()).addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent g) {
					GreedyStrategy greedy = new GreedyStrategy();
					theGame.getComputerPlayer().setStrategy(greedy);
					}
			});
			
			sComputerPlayer.add(this.randomItem()).addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent c) {
					RandomStrategy random = new RandomStrategy();
					theGame.getComputerPlayer().setStrategy(random);
					}
				});
			return sComputerPlayer;
	}
			
	/**
	 * This helper method creates the cautious menu item and should be added to the computer player menu
	 * @return the cautiousItem
	 */
	private JMenuItem cautiousItem() {
			JMenuItem sCautious = new JMenuItem("Cautious");
			sCautious.setMnemonic(KeyEvent.VK_C);
			

			return sCautious;
	}
	/**
	 * This helper method creates the greedy menu item and should be added to the computer player menu	
	 */
		private JMenuItem greedyItem() {
			JMenuItem sGreedy = new JMenuItem("Greedy");
			sGreedy.setMnemonic(KeyEvent.VK_G);
			//sComputerPlayer.add(sGreedy);
			return sGreedy;
		}
	/**
	 * This helper method creates the random menu item and should be added to the computer player menu
	 */
		private JMenuItem randomItem() {
		JMenuItem sRandom = new JMenuItem("Random");
		sRandom.setMnemonic(KeyEvent.VK_R);


		return sRandom;
	}
		
		
		private void buildContentPane() {
		
		this.pnlChooseFirstPlayer = 
				new NewGamePanel(this.theGame);
		this.contentPane.add(this.pnlChooseFirstPlayer, "North");
		
		// TODO: 1. Instantiate this.pnlHumanPlayer, add it to
		//		the content pane at the left, and disable it.
		this.pnlHumanPlayer = new HumanPlayerPanel(this.theGame);
		this.contentPane.add(this.pnlHumanPlayer, "West");
		this.pnlHumanPlayer.setEnabled(false);
		
		//		 2. Instantiate this.pnlGameInfo and add it to
		//			the content pane in the middle.
		this.pnlGameInfo = new GameStatusPanel(this.theGame);
		this.contentPane.add(this.pnlGameInfo, "Center");

		//		 3. Instantiate this.pnlComputerPlayer, add it to
		//			the content pane at the right, and disable it.
		
		this.pnlComputerPlayer = new ComputerPlayerPanel(this.theGame);
		this.contentPane.add(this.pnlComputerPlayer, "East");
		this.pnlComputerPlayer.setEnabled(false);
		
		
	}
	
	
	
	//************************* private inner classes *************************
	
	/*
	 * Defines the panel in which the user selects which Player plays first.
	 */
	private final class NewGamePanel extends JPanel {

		private static final long serialVersionUID = 140604L;
		
		 private JRadioButton radHumanPlayer;
		 private JRadioButton radComputerPlayer;
		 private JButton randomButton;
		
		private Game theGame;
		private Player theHuman;
		private Player theComputer;

		private NewGamePanel(Game theGame) {
			this.theGame = theGame;
			
			this.theHuman = this.theGame.getHumanPlayer();
			this.theComputer = this.theGame.getComputerPlayer();
			
			
			this.buildPanel();
		}
		
		private void buildPanel() {
			this.setBorder(BorderFactory.createTitledBorder("Select first player"));	
			
			// TODO: Instantiate this.btnComputerPlayer and add 
						//		 ComputerFirstListener as its action listener.
			this.radHumanPlayer = new JRadioButton("Human first");	
			this.radHumanPlayer.addActionListener(new HumanFirstListener());
			
			this.radComputerPlayer = new JRadioButton("Computer first");
			this.radComputerPlayer.addActionListener(new ComputerFirstListener());
			
			// Random button that selects a radio button for the user
			final Random random = new Random();
			this.randomButton = new JButton("Random");
			randomButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent rand) {
					int n = random.nextInt(2);
						if(n == 0) {
							radComputerPlayer.doClick();
							
						}else{
							radHumanPlayer.doClick();
						}
					
				}
				
			});
			
			
			
			// TODO: Create a ButtonGroup and add the 2 radio buttons to it.
			ButtonGroup buttons = new ButtonGroup();	
			buttons.add(this.radHumanPlayer);
			buttons.add(this.radComputerPlayer);
			
			
			// TODO: Add the 2 radio buttons to this panel.
			add(this.radHumanPlayer);
			add(this.radComputerPlayer);
			add(this.randomButton);
		}
		
		/* 
		 * Defines the listener for computerPlayerButton.
		 */		
		private class ComputerFirstListener implements ActionListener {
			
			@Override
			/** 
			 * Enables the ComputerPlayerPanel and starts a new game. 
			 * Event handler for a click in the computerPlayerButton.
			 */
			
			public void actionPerformed(ActionEvent eventObject) {
					
				Gui.this.pnlComputerPlayer.setEnabled(true);
				
				
				Gui.this.theGame.startNewGame(NewGamePanel.this.theComputer, NewGamePanel.this.theHuman);
				
				ComputerPlayerPanel.getTurnButton().doClick();
			}
		}

		
		/* 
		 * Defines the listener for humanPlayerButton.
		 */	
		private class HumanFirstListener implements ActionListener {

			/* 
			 * Enables the HumanPlayerPanel and starts a new game. 
			 * Event handler for a click in the humanPlayerButton.
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO: Enable pnlHumanPlayer and start a game
				//		 with theHuman playing first.
				Gui.this.pnlHumanPlayer.setEnabled(true);
				Gui.NewGamePanel.this.theGame.startNewGame(Gui.NewGamePanel.this.theHuman, Gui.NewGamePanel.this.theComputer);
				ComputerPlayerPanel.getTurnButton().doClick();
				
				}
			}
	
	}
}

