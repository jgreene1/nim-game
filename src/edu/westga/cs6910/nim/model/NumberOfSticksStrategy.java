package edu.westga.cs6910.nim.model;
/**
 * The purpose of this interface is to define the common interface for all the game-play algorithms for the 1-pile version of Nim. 
 * @author Jonathan Greene	
 * @version 6/22/2014
 *
 */
public interface NumberOfSticksStrategy {
	
	/**
	 * 
	 * @return the number of sticks to be taken from the Pile
	 * @return the number of sticks to take, an integer that is > 0 and <= the pile size
	 * @precondition pileSize > 0
	 * 
	 */
	public int howManySticks(Pile pileSize);
	// changed int to Pile to see if that would fix the problem
}
