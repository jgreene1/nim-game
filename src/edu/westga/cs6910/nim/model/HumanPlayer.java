package edu.westga.cs6910.nim.model;

/**
 * HumanPlayer represents a human player in the game Nim.
 * 
 * @author CS 6910 
 * @author Jonathan Greene
 * @version Summer 2014
 */
public class HumanPlayer extends AbstractPlayer {

	private String name;
	private Pile thePile;

	public HumanPlayer(String name, int sticksToTake, Pile thePile) {
		super(name, sticksToTake, thePile);
		this.sticksToTake = sticksToTake;
	    this.name = name;
	   //check constructors as parameters may not be needed.
	  }

	
	@Override
	/**
	 * Implements Player's setNumberSticksToTake() to set the number
	 * of sticks to the maximum allowed for this turn.
	 * 
	 * @ensure  sticksOnThisTurn() == 
	 * 					Math.min(pileForThisTurn.sticksLeft()-1, 
	 * 							 Game.MAX_STICKS_PER_TURN)
	 * 
	 * @see Player#setNumberSticksToTake()
	 */
	public void setNumberSticksToTake() {
		super.setNumberSticksToTake(Math.min(this.thePile.getSticksLeft() - 1, Game.MAX_STICKS_PER_TURN));
			
	}

	
	
	/**
	 * Returns a String representation of this HumanPlayer by giving its name 
	 * 	and number of sticks to take
	 * 
	 * @return	A String representation of the object
	 */
	public String toString() {
		return "Human Player named: " + this.name
				+ " with " + super.sticksToTake + " sticks to take";
	}
}
