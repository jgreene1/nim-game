package edu.westga.cs6910.nim.model;

public abstract class AbstractPlayer implements Player {
	public String name;
	public int sticksToTake;
	public Pile thePile;

	public AbstractPlayer(String name, int sticksToTake, Pile thePile) {
		this.name = name;
		this.sticksToTake = sticksToTake;
		this.thePile = thePile;
	}
	
	
	/* (non-Javadoc)
	 * @see edu.westga.cs6910.nim.model.Player#setPileForThisTurn(edu.westga.cs6910.nim.model.Pile)
	 */
	@Override
	public void setPileForThisTurn(Pile aPile) {
		this.thePile = aPile;
		
	}

	/* (non-Javadoc)
	 * @see edu.westga.cs6910.nim.model.Player#setNumberSticksToTake(int)
	 */
	@Override
	public void setNumberSticksToTake(int number) {
		this.sticksToTake = number;
	}

	/* (non-Javadoc)
	 * @see edu.westga.cs6910.nim.model.Player#takeTurn()
	 */
	@Override
	public void takeTurn(Pile pile) {
		this.thePile = pile;
		pile.removeSticks(this.sticksToTake);
		
	}

	/* (non-Javadoc)
	 * @see edu.westga.cs6910.nim.model.Player#getName()
	 */
	@Override
	public String getName() {
		return this.name;

	}

	/* (non-Javadoc)
	 * @see edu.westga.cs6910.nim.model.Player#getSticksOnThisTurn()
	 */
	@Override
	public int getSticksOnThisTurn() {
		return this.sticksToTake;
	}

	/* (non-Javadoc)
	 * @see edu.westga.cs6910.nim.model.Player#getPileForThisTurn()
	 */
	@Override
	public Pile getPileForThisTurn() {
		  return this.thePile;
	}



}