package edu.westga.cs6910.nim.model;

// TODO: Classes ComputerPlayer and HumanPlayer share most of their code.
//		 Refactor their code:
// 		 1. Create abstract base class AbstractPlayer to implement the
//			shared code and define abstract methods for methods without
//			duplicate code. AbstractPlayer should implement interface Player.
//		 2. Have ComputerPlayer and HumanPlayer extend AbstractPlayer to
//		    implement the unshared constructor code and the abstract methods.

/**
 * ComputerPlayer represents a very simple automated player in the game Nim.
 * It removes 1 stick at a time.
 * 
 * @author CS 6910 
 * @author Jonathan Greene
 * @version Summer 2014
 */
public class ComputerPlayer extends AbstractPlayer {
	
	private static final String NAME = "Simple computer";
	private Pile thePile;
	private NumberOfSticksStrategy numSticksStrat;
	

	/**
	 * Creates a new ComputerPlayer with the specified name.
	 * 
	 */
	public ComputerPlayer(int sticksToTake, Pile thePile) {
		super(NAME, sticksToTake, thePile);
		this.name = NAME;
		this.thePile = thePile;
		CautiousStrategy cautious = new CautiousStrategy();
		this.numSticksStrat = cautious;
		//Does not assign a value to the instance variable, sticksToTake
		//change the constructor superclass
	}	
	
	
	
	/**
	 * This constructor checks to make sure strategy is not null 
	 */
	//need test for this constructor once it is fixed
	public ComputerPlayer(NumberOfSticksStrategy numOfSticksStrat, Pile thePile) {
		super(NAME, 0, thePile);
		this.name = NAME;
		if(numOfSticksStrat == null){
			throw new IllegalArgumentException("The strategy can not be null");
		}
		this.numSticksStrat = numOfSticksStrat;
		this.thePile = thePile;
		this.sticksToTake = this.numSticksStrat.howManySticks(this.thePile);
	}




	//*************************** mutator methods ****************************
	
	
	@Override	
	/**
	 * Implements Player's setNumberSticksToTake() to set the number
	 * of sticks to 1.
	 * 
	 * @ensure  sticksOnThisTurn() == 1
	 * 
	 * @see Player#setNumberSticksToTake()
	 * @postcondition number of sticks on this turn will be a number between 1 and
	 * the most sticks that can be legally taken, inclusive
	 * sticksOnThisTurn() >= 1 && sticksOnThisturn() <= Math.min(pileForThisTurn.sticksLeft()-1, Game.MAX_STICKS_PER_TURN
	 */

	public void setNumberSticksToTake() {
		if (getSticksOnThisTurn() >= 1 && getSticksOnThisTurn() <= Math.min(getPileForThisTurn().getSticksLeft()-1, Game.MAX_STICKS_PER_TURN)) {
			this.sticksToTake = numSticksStrat.howManySticks(this.thePile);	
		} else {
			this.sticksToTake = 1;
		}
		
		
	}
	
	/**
	 * @precondition the parameter is not null
	 * @postcondition the specified strategy will determine how many sticks to take
	 */
	public void setStrategy(NumberOfSticksStrategy numOfSticksStrat) {
		if (numOfSticksStrat == null) {
			throw new IllegalArgumentException("Strategy can not be null");
		}
		this.numSticksStrat = numOfSticksStrat;
	}

	
	
	//*************************** accessor methods ****************************

	
	/**
	 * Returns a String representation of this ComputerPlayer by giving its name 
	 * 	and number of sticks to take
	 * 
	 * @return	A String representation of the object
	 */
	public String toString() {
		return "Computer Player named: " + this.name
				+ " with " + super.sticksToTake + " sticks to take";
	}


}
