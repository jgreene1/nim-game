package edu.westga.cs6910.nim.model;

import java.util.Random;

/**
 * This class will implement the game-play strategy that returns a random number
 * between 1 and the maximum number allowed inclusive
 * @author Jonathan Greene
 * @version 6/23/2014
 *
 */
public class RandomStrategy implements NumberOfSticksStrategy {
	
	public Random random;
	
	
	/**
	 * This is constructor for the RandomStrategy
	 */
	public RandomStrategy() {
		this.random = new Random();
		
	}


	/**
	 * this method will generate a random number to be taken from the pile
	 */
	@Override
	public int howManySticks(Pile thePile) {
		if (thePile.getSticksLeft() <= 0) {
			throw new IllegalArgumentException("Pile size must be greater than 0");
		}
		this.random = new Random();
		int num = random.nextInt(Math.min(thePile.getSticksLeft()-1, Game.MAX_STICKS_PER_TURN)) + 1;
		return num;
	}
	

}
