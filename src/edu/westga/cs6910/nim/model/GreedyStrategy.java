package edu.westga.cs6910.nim.model;

public class GreedyStrategy implements NumberOfSticksStrategy {
	
	@Override
	public int howManySticks(Pile thePile) {
		if (thePile.getSticksLeft() >= 4){
			return 3;
		} else {
			return thePile.getSticksLeft() - 1;
		}
	}
	

}
