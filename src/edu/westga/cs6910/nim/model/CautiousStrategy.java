package edu.westga.cs6910.nim.model;
/**
 * This class will implement the game-play strategy that always returns 1 as the number
 * of sticks to be taken from the Pile
 * @author Jonathan Greene
 * @version 6/22/2014
 *
 */
public class CautiousStrategy implements NumberOfSticksStrategy {
	public Pile thePile;

	/**
	 * @return returns the number of sticks to take, 1
	 * @precondition pileSize > 0 
	 */
	@Override
	public int howManySticks(Pile thePile) {
		this.thePile = thePile;
		if (thePile.getSticksLeft() <= 0) {
			throw new IllegalArgumentException("Pile size must be greater than 0");
		}
		return 1;
	}

}
